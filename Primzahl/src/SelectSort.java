
    public class SelectSort {
        public static int verg = 0;
        public static int vert = 0;

        public static void main(String[] args) {

            int[] unsorted = { 4, 6, 8, 3, 1, 9, 7 };
            int[] sorted = selectionsort(unsorted);

            for (int i = 0; i < sorted.length; i++) {
                if (i == sorted.length - 1) {
                    System.out.print(sorted[i]);
                } else {
                    System.out.print(sorted[i] + ", ");
                }
            }
            System.out.println("");
            System.out.println("Sortiert:" + vert);
            System.out.println("Vergleicht: " + verg);

        }

        public static int[] selectionsort(int[] sortieren) {
            for (int i = 0; i < sortieren.length - 1; i++) {
                for (int j = i + 1; j < sortieren.length; j++) {
                    verg++;
                    if (sortieren[i] > sortieren[j]) {
                        int temp = sortieren[i];
                        sortieren[i] = sortieren[j];
                        sortieren[j] = temp;
                        vert++;
                    }
                    for (int s = 0; s < sortieren.length; s++) {
                        if (s == sortieren.length - 1) {
                            System.out.print(sortieren[s]);
                        } else {
                            System.out.print(sortieren[s] + ", ");
                        }
                    }
                    System.out.println("");
                }
            }

            return sortieren;
        }
    }
