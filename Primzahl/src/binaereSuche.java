
public class binaereSuche {

	
	public long binaeresuche(long zahlen[], long gesuchteZahl) {
		long zahl1 = 0;
		long zahl2 = zahlen.length - 1;
		long versuche = 0;
		
		while (zahl1 <= zahl2) {

			long mitte = zahl1 + ((zahl2 - zahl1) / 2);

			if (gesuchteZahl == zahlen[(int) mitte]) {
				System.out.println("Anzahl der versuche:" + versuche);
				return mitte;
			} else {
				if (zahlen[(int) mitte] > gesuchteZahl) {
					zahl2 = mitte - 1;
					versuche++;
				} else  {
					zahl1 = mitte + 1;
					versuche++;
				}
			}
			
		}
		System.out.println("Anzahl der versuche:" + versuche);
		return -1;
		
	}
}


