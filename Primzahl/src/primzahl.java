import java.util.Scanner;
import java.util.Random;
public class primzahl {
	

	public static void main(String[] args) {
		
		
		
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("Geben Sie eine Zahl ein!");
		long zahl = sc.nextLong();
		long start = System.currentTimeMillis();
		System.out.println("ist das eine Primzahl?: " + isPrimzahl(zahl));
		long ende = System.currentTimeMillis();
		long zeit = ende - start;

		System.out.println("Dauer: " + zeit + " " + "Millisekunden");
		System.out.println();
		
		
	}


	public static boolean isPrimzahl(long zahl) {

		boolean isPrimzahl = true;
		for (long i = zahl - 1; i > 1; i--) {
			int mudolo = (int) (zahl % i);
			if (mudolo == 0) {
				i = 0;
				isPrimzahl = false;
			}
		}
		return isPrimzahl;
		
	}

			

		
}
