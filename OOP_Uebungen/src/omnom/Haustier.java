package omnom;

public class Haustier {
	private String name;
	private int hunger;
	private int muede;
	private int zufrieden;
	private int gesund;

	// Konstruktor
	public Haustier() {

	}

	public Haustier(String name) {
		name = name;
		hunger = 100;
		muede = 100;
		zufrieden = 100;
		gesund = 100;

	}

	// Verwaltungsmethoden
	public void heilen() {
		gesund = 100;
	}

	public void schlafen(int schlafen) {
		muede += schlafen;
		if (muede > 100) {
			muede = 100;
		}
	}

	public void spielen(int spielen) {
		zufrieden += spielen;
		if (zufrieden > 100) {
			zufrieden = 100;
		}

	}

	public void fuettern(int futtern) {
		hunger += futtern;
		if (hunger > 100) {
			hunger = 100;
		}
	}

	// Getter und Setter
	public String getName() {
		return name;
	}

	public void setName(String name) {

		this.name = name;

	}

	public int getHunger() {
		return hunger;
	}

	public void setHunger(int hunger) {
		if (!(hunger < 0 && hunger > 100)) {
			this.hunger = hunger; // ‹berschreiben
		}

	}

	public int getMuede() {
		return muede;
	}

	public void setMuede(int muede) {
		if (!(muede < 0 && muede > 100)) {

			this.muede = muede;
		}
	}

	public int getZufrieden() {

		return zufrieden;
	}

	public void setZufrieden(int zufrieden) {
		if (!(zufrieden < 0 && zufrieden > 100)) {
			this.zufrieden = zufrieden;
		}
	}

	public int getGesund() {
		return gesund;
	}

	public void setGesund(int gesund) {
		if (!(gesund < 0 && gesund > 100)) {
			this.gesund = gesund;

		}

	}
}