package de.futurehome.tanksimulator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyActionListener implements ActionListener {
	public TankSimulator f;

	public MyActionListener(TankSimulator f) {
		this.f = f;
	}

	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if (obj == f.btnBeenden)
			System.exit(0);

		if (obj == f.btnEinfuellen) {
			int fuellstand = f.myTank.getFuellstand();

			if (fuellstand <= 95) {
				fuellstand = fuellstand + 5;

			} else if (fuellstand > 95) {
				fuellstand = 100;
			}

			f.myTank.setFuellstand(fuellstand);
			f.lblFuellstandp.setText(fuellstand + "%");
			f.log("5 Liter wurden getankt");
			f.lblFuellstand.setText("" + fuellstand);
		}
		if (obj == f.btnVerbrauchen) {
			int verbrauchen = f.myTank.getFuellstand();
			
			if (verbrauchen >= 2) {
				verbrauchen = verbrauchen - 2;

			} else if (verbrauchen < 2) {
				verbrauchen = 0;
		}
			f.myTank.setFuellstand(verbrauchen);
			f.log("2 Liter wurden verbraucht");
			f.lblFuellstand.setText("" + verbrauchen);
			f.lblFuellstandp.setText(verbrauchen + "%");
		}

	if(obj==f.btnZuruecksetzen)

	{

		int zuruecksetzen = f.myTank.getFuellstand();
		zuruecksetzen = 0;
		f.myTank.setFuellstand(zuruecksetzen);
		f.log("Tank wurde geleert");
		f.lblFuellstand.setText("" + zuruecksetzen);
		f.lblFuellstandp.setText(zuruecksetzen + "%");

	}

}

}
