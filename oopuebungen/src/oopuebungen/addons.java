package oopuebungen;

public class addons {

	private int maxAnzahl;
	private int anzahlAddons;
	private int idNummer;
	private String bezeichnung;
	private double verkaufspreis;

	public addons() {
	}

	public int getMaxAnzahl() {

		return maxAnzahl;
	}

	public void setMaxAnzahl(int maxAnzahl) {
		this.maxAnzahl = maxAnzahl;
	}

	public int getAnzahlwidhAddons() {

		return anzahlAddons;
	}

	public void setAnzahlwidhAddons(int anzahlwidhAddons) {
		this.anzahlAddons = anzahlwidhAddons;
	}

	public int getIdNummer() {
		return idNummer;
	}

	public void setIdNummer(int idNummer) {
		this.idNummer = idNummer;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public double getVerkaufspreis() {
		return verkaufspreis;
	}

	public void setVerkaufspreis(double verkaufspreis) {
		this.verkaufspreis = verkaufspreis;
	}

	public double gesWert() {
		return verkaufspreis * anzahlAddons;
	}

	public void veraendereBestand(int veraendereBestand) {
		anzahlAddons = veraendereBestand;
	}
}
