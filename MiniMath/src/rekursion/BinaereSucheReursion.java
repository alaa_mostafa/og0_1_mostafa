package rekursion;

public class BinaereSucheReursion {

	int binaereSuscheRekursion(int zahlen[], int rechts, int links, int gesuchteZahl) {

		if (links >= rechts && rechts < zahlen.length - 1) {
			int mitte = rechts + (links - rechts) / 2;

			if (zahlen[mitte] == gesuchteZahl)
				return mitte;
			if (zahlen[mitte] > gesuchteZahl)

				return binaereSuscheRekursion(zahlen, rechts, mitte - 1, gesuchteZahl);
			return binaereSuscheRekursion(zahlen, mitte + 1, links, gesuchteZahl);
		}
		return zahlen.length -1;
	}

}

