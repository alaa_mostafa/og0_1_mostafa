package oszkickers;

public class PersonTest {

	public static void main(String[] args) {

		Person person1 = new Person();

		person1.setName("Alaa Mostafa");
		person1.setTelefonnummer("017624407274");
		person1.setJahresbetragBezahlt(true);

		Trainer trainer1 = new Trainer();
		
		trainer1.setName("J�rgen");
		trainer1.setTelefonnummer("01761234567");
		trainer1.setJahresbetragBezahlt(true);
		trainer1.setLizenzklasse('A');
		trainer1.setAufwandEntschaedigung(300);

		
		Spieler spieler1 = new Spieler();
		
		spieler1.setName("Messi");
		spieler1.setTelefonnummer("01767654321");
		spieler1.setJahresbetragBezahlt(true);
		spieler1.setTrikotnummer(10);
		spieler1.setSpielePosition("St�rmer");
		

		Schiedsrichter sch1 = new Schiedsrichter();
		
		sch1.setName("Peter");
		sch1.setTelefonnummer("017634534785");
		sch1.setJahresbetragBezahlt(true);
		sch1.setAnzgepiftSpieler(2);
		sch1.setNamegepiftSpieler("Messi" + ", " + "Ronaldo");
		

		Manschaftsleiter mansch1 = new Manschaftsleiter();
		
		mansch1.setName("Neuer");
		mansch1.setTelefonnummer("01761263442");
		mansch1.setJahresbetragBezahlt(true);
		mansch1.setManschaftsName("FC Barcelona");
		mansch1.setJahresBetragRabatt(25.5);

		System.out.println("");
		System.out.println("---------------------------------------------");

		System.out.println("Mitgliederverwaltung - OSZ KICKERS TORNADO 1996: ");

		System.out.println("");



		System.out.println("");

		System.out.println("Trainer�bersicht:");
		System.out.println("Name: " + trainer1.getName());
		System.out.println("Telefonnummer: " + trainer1.getTelefonnummer());
		System.out.println("Jahesbetrag?: " + trainer1.getJahresbetragBezahlt());
		System.out.println("Lizenz: " + trainer1.getLizenzklasse());
		System.out.println("Aufwandentsch�digung: " + trainer1.getAufwandEntschaedigung() + "�");

		System.out.println("");
		System.out.println("Spieler�bersicht");
		System.out.println("Name: " + spieler1.getName());
		System.out.println("Telefonnumer: " + spieler1.getTelefonnummer());
		System.out.println("Jahresbetrag?: " + spieler1.getJahresbetragBezahlt());
		System.out.println("Trikotnummer: " + spieler1.getTrikotnummer());
		System.out.println("Spielerposition:" + spieler1.getSpielePosition());

		System.out.println("");

		System.out.println("Schiedsrichter�bersicht:");
		System.out.println("Name: " + sch1.getName());
		System.out.println("Telefonnummer: " + sch1.getTelefonnummer());
		System.out.println("Jahresbetrag bezahlt?: " + sch1.getJahresbetragBezahlt());
		System.out.println("Anzahl der gepiften Spieler: " + sch1.getAnzgepiftSpieler());
		System.out.println("Name der gepiften Spieler: " + sch1.getNamegepiftSpieler());
		
		System.out.println("");
		
		System.out.println("Manschaftsleiter:");
		System.out.println("Name: " + mansch1.getName());
		System.out.println("Telefonnummer: " + mansch1.getTelefonnummer());
		System.out.println("Jahresbetrag?: " + mansch1.getJahresbetragBezahlt());
		System.out.println("Manschaftsname: " + mansch1.getManschaftsName());
		System.out.println("Jahresbetrag Rabatt je nach pers�nlichem Engagement:" + mansch1.getJahresBetragRabatt());
		
		System.out.println("---------------------------------------------");
	}

}