package oszkickers;

public class Schiedsrichter extends Person {
	/** Atribute **/
	private int anzgepiftSpieler;
	private String namegepiftSpieler;
	
	/** Konstruktoren **/

	public Schiedsrichter() {
		
	}

	public Schiedsrichter(String name, String telefonnummer, boolean jahresbetragBezahlt, int anzgepiftSpieler, String namegepiftSpieler) {
		
		super(name, telefonnummer, jahresbetragBezahlt);
		
		this.anzgepiftSpieler = anzgepiftSpieler;
		this.namegepiftSpieler = namegepiftSpieler;
	}

	/** Verwaltungsmethoden **/

	public int getAnzgepiftSpieler() {
		return anzgepiftSpieler;
	}

	public void setAnzgepiftSpieler(int anzgepiftSpieler) {
		this.anzgepiftSpieler = anzgepiftSpieler;
	}

	public String getNamegepiftSpieler() {
		return namegepiftSpieler;
	}

	public void setNamegepiftSpieler(String namegepiftSpieler) {
		this.namegepiftSpieler = namegepiftSpieler;
	}
	
	
}
