package oszkickers;

public class Manschaftsleiter extends Person {
	/** Atribute **/
	private String manschaftsName;
	private double jahresBetragRabatt;

	/** Konstruktoren **/

	public Manschaftsleiter(){
		
	}
	public Manschaftsleiter(String name, String telefonnummer, boolean jahresbetragBezahlt, String manschaftsName, double jahresBetragRabatt) {
		
		super(name, telefonnummer, jahresbetragBezahlt);
		
		this.manschaftsName = manschaftsName;
		this.jahresBetragRabatt = jahresBetragRabatt;

	}

	/** Verwaltungsmethoden **/

	public String getManschaftsName() {
		return manschaftsName;
	}

	public void setManschaftsName(String manschaftsName) {
		this.manschaftsName = manschaftsName;
	}

	public double getJahresBetragRabatt() {
		return jahresBetragRabatt;
	}

	public void setJahresBetragRabatt(double jahresBetragRabatt) {
		this.jahresBetragRabatt = jahresBetragRabatt;
	}

}
