package oszkickers;

public class Person {
	/** Atribute **/
	private String name;
	private String telefonnummer;
	private boolean jahresbetragBezahlt;

	/** Konstruktoren **/
	public Person() {
	}

	public Person(String name, String telefonnummer, boolean jahresbetragBezahlt) {
		this.name = name;
		this.telefonnummer = telefonnummer;
		this.jahresbetragBezahlt = jahresbetragBezahlt;
	}
	/** Verwaltungsmethoden **/

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTelefonnummer() {
		return telefonnummer;
	}

	public void setTelefonnummer(String telefonnummer) {
		this.telefonnummer = telefonnummer;
	}

	public boolean getJahresbetragBezahlt() {
		return jahresbetragBezahlt;
	}

	public void setJahresbetragBezahlt(boolean jahresbetragBezahlt) {
		this.jahresbetragBezahlt = jahresbetragBezahlt;
	}
	
}
