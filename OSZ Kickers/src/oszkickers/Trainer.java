package oszkickers;

public class Trainer extends Person {
	/** Atribute **/
	private char lizenzklasse;
	private int aufwandEntschaedigung;
	
	/** Konstruktoren **/

	public Trainer() {
	}

	public Trainer(String name, String telefonnummer, boolean jahresbetragBezahlt, char lizenzklasse, int aufwandEntschaedigung) {
		
		super(name, telefonnummer, jahresbetragBezahlt);
		
		this.lizenzklasse = lizenzklasse;
		this.aufwandEntschaedigung = aufwandEntschaedigung;
	}

	/** Verwaltungsmethoden **/

	public char getLizenzklasse() {
		return lizenzklasse;
	}

	public void setLizenzklasse(char lizenzklasse) {
		this.lizenzklasse = lizenzklasse;
	}

	public int getAufwandEntschaedigung() {
		return aufwandEntschaedigung;
	}

	public void setAufwandEntschaedigung(int aufwandEntschaedigung) {
		this.aufwandEntschaedigung = aufwandEntschaedigung;
	}

}
