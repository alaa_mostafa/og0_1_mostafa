package oszkickers;

public class Spieler extends Person {
	/** Atribute **/
	private int trikotnummer;
	private String spielePosition;

	/** Konstruktoren **/

	public Spieler() {

	}

	public Spieler(String name, String telefonnummer, boolean jahresbetragBezahlt, int trikotnummer, String spielePosition) {
		
		super(name, telefonnummer, jahresbetragBezahlt);
		
		this.trikotnummer = trikotnummer;
		this.spielePosition = spielePosition;
	}

	/** Verwaltungsmethoden **/

	public int getTrikotnummer() {
		return trikotnummer;
	}

	public void setTrikotnummer(int trikotnummer) {
		this.trikotnummer = trikotnummer;
	}

	public String getSpielePosition() {
		return spielePosition;
	}

	public void setSpielePosition(String spielePosition) {
		this.spielePosition = spielePosition;
	}
	
}
