package de.promocode.logic;

public class CodeSearch {

	public static int NOT_FOUND = -1;


	public static int findPromoCode(long[] list, long zuSuchen) {
		long max = list.length - 1;
		long min = 0;
		while (zuSuchen >= list[(int) min] && zuSuchen <= list[(int) max]) {

			long pos = min + (max - min) * (zuSuchen - list[(int) min]) / (list[(int) max] - list[(int) min]);
			if (list[(int) pos] == zuSuchen) {
				System.out.println("Gefunden bei: " + pos);
				return (int) pos;
			} else if (list[(int) pos] < zuSuchen) {
				min = pos + 1;
			} else {
				max = pos - 1;
			}

	}
		return NOT_FOUND;

}
}
