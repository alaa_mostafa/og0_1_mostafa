import java.util.ArrayList;

public class ArrayListUebung {

	public static void main(String[] args) {
		
		// TODO Auto-generated method stub
		ArrayList<Integer> liste = new ArrayList<>();
		liste.add(5);
		liste.add(10);
		liste.add(15);
		liste.add(20);
		System.out.println(liste);
		
		liste.remove(2);
		System.out.println(liste);

		System.out.println(liste.get(1));
		System.out.println(liste);
		
		liste.clear();
		System.out.println(liste);
	}

}
